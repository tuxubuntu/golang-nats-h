package nats

import "log"
import "time"

import "github.com/nats-io/go-nats"
import "github.com/vmihailenco/msgpack"


type Server struct {
	conn *nats.Conn
	T int
}

func(self *Server) Listen(name string, handler func([]byte)[]byte) error {
	self.conn.Subscribe(name, func(msg *nats.Msg) {
		reply := handler(msg.Data)
		self.conn.Publish(msg.Reply, reply)
	})
	self.conn.Flush()
	if err := self.conn.LastError(); err != nil {
		return err
	}
	log.Printf("Listening on [%s]\n", name)
	return nil
}

func(self *Server) Call(name string, payload []byte) ([]byte, error) {
	msg, err := self.conn.Request(name, payload, time.Duration(self.T)*time.Millisecond)
	if err != nil {
		if self.conn.LastError() != nil {
			return nil, self.conn.LastError()
		}
		return nil, err
	}
	return msg.Data, nil
}

func Marshal(v ...interface{}) ([]byte, error) {
	body, err := msgpack.Marshal(v...)
	return body, err
}

func Unmarshal(b []byte, v ...interface{}) error {
	err := msgpack.Unmarshal(b, v...)
	return err
}

func(self *Server) Close() {
	self.conn.Close()
}

func NewServer(address string) (*Server, error) {
	nc, err := nats.Connect(address)
	if err != nil {
		return nil, err
	}
	self := Server{
		conn: nc,
		T: 100,
	}
	return &self, nil
}
